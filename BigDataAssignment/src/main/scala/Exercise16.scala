import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object Exercise16 extends App {

  // use if cannot find hadoop winutils environment variable
  //  System.setProperty("hadoop.home.dir", "C:\\tools\\winutils-master (hadoop)\\winutils-master\\hadoop-2.7.1") home computer
  //  System.setProperty("hadoop.home.dir", "C:\\tools\\hadoop-2.7.1") ies computer

  Logger.getLogger("org").setLevel(Level.ERROR)
  val spark = SparkSession.builder().
    master("local").getOrCreate()
  import spark.implicits._

  spark.sparkContext.setLogLevel("ERROR");

  val studentsJson = spark.read.json("datos/students.json")

  studentsJson.select(col("*"),explode($"subjects").as("subject")).drop("subjects")
    .groupBy($"subject.name")
    .agg(count("subject.name").as("NumberStudents"))
    .select(col("name").as("Subject"), col("NumberStudents"))
    .show(false)

}
