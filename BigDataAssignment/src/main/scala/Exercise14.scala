import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object Exercise14 extends App {

  // use if cannot find hadoop winutils environment variable
  //  System.setProperty("hadoop.home.dir", "C:\\tools\\winutils-master (hadoop)\\winutils-master\\hadoop-2.7.1") home computer
//  System.setProperty("hadoop.home.dir", "C:\\tools\\hadoop-2.7.1") ies computer

  Logger.getLogger("org").setLevel(Level.ERROR)
  val spark = SparkSession.builder().
    master("local").getOrCreate()

  spark.sparkContext.setLogLevel("ERROR");

  val teachersJson = spark.read.json("datos/teachers.json")

  teachersJson.select(col("fullname"), size(col("subjects")).as("NumberSubjects"))
    .show()

}
