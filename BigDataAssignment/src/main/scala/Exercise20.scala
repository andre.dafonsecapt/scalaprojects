import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object Exercise20 extends App {

  // use if cannot find hadoop winutils environment variable
  //  System.setProperty("hadoop.home.dir", "C:\\tools\\winutils-master (hadoop)\\winutils-master\\hadoop-2.7.1") home computer
  //  System.setProperty("hadoop.home.dir", "C:\\tools\\hadoop-2.7.1") ies computer

  Logger.getLogger("org").setLevel(Level.ERROR)
  val spark = SparkSession.builder().
    master("local").getOrCreate()

  import spark.implicits._

  spark.sparkContext.setLogLevel("ERROR");

  val studentsJson = spark.read.json("datos/students.json")

  studentsJson.withColumn("NumberSubjects", size(col("subjects")))
    .select(col("*"), explode($"subjects").as("subject")).drop("subjects")
    .withColumn("calls", $"subject.calls").withColumn("SubjectName", $"subject.name")
    .select(col("*"), element_at(reverse(array_sort($"calls")), 1).as("lastMarks"))
    .withColumn("call", col("lastMarks.call.$numberInt").cast("Int"))
    .withColumn("mark", col("lastMarks.mark.$numberInt").cast("Int"))
    .filter(col("mark").geq(5))
    .groupBy($"fullname", $"NumberSubjects")
    .agg(count("fullname").as("NumberPassedSubjects"), avg($"mark").as("MarksAverage"))
    .filter(col("NumberSubjects").equalTo(col("NumberPassedSubjects")))
    .select(first(col("fullname"), false).as("FullName"), max(col("MarksAverage")).as("MarksAverage"))
    .show(false)

}
