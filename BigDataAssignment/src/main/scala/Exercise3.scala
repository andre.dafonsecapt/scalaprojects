import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, substring_index}

object Exercise3 extends App {

  // use if cannot find hadoop winutils environment variable
  //  System.setProperty("hadoop.home.dir", "C:\\tools\\winutils-master (hadoop)\\winutils-master\\hadoop-2.7.1") home computer
  //  System.setProperty("hadoop.home.dir", "C:\\tools\\hadoop-2.7.1") ies computer

  Logger.getLogger("org").setLevel(Level.ERROR)
  val spark = SparkSession.builder().
    master("local").getOrCreate()

  import spark.implicits._

  spark.sparkContext.setLogLevel("ERROR");

  val madrid = spark.read.option("header", "true").option("encoding", "windows-1252").csv("datos/206974-0-agenda-eventos-culturales-100.csv")

  madrid.filter(col("NOMBRE-INSTALACION").like("%(Latina)%"))
    .withColumn("TIPO", substring_index($"TIPO", "/", -1))
    .select(col("TITULO"), col("TIPO")).show();

}
